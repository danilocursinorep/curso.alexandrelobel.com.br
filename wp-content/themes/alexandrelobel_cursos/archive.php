<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Cursos_|_Alexandre_Lobel
 */

header('Location: ' . home_url());