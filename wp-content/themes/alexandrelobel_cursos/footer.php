<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Cursos_|_Alexandre_Lobel
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 align-self-center">
              			<a href="https://alexandrelobel.com.br" target="_self">
              				<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" />
              			</a>
				</div>
				<div class="col-12 col-lg-6 align-self-center">
					<ul>
						<li><a href="<?php echo home_url('politica-de-privacidade'); ?>">Política de privacidade</a></li>
						<li><a href="<?php echo home_url('consideracoes-importantes'); ?>">Considerações importantes</a></li>
						<li><a href="<?php echo home_url('termos-de-uso'); ?>">Termos de uso</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<p>© 2020 Dr. Alexandre Lobel. Todos os direitos reservados.</p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2253636238024646');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2253636238024646&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</html>