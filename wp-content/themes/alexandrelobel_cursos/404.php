<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Cursos_|_Alexandre_Lobel
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="page404">

				<div class="container">
					<div class="row">
						<div class="col-12">
							<h2>Erro 404</h2>
							<h3>Página não encontrada!</h3>
							<a href="<?php echo home_url(); ?>"><p>Voltar para a Página incial</p></a>
							<a href="https://alexandrelobel.com.br"><p>Ir para o site do Dr. Alexandre Lobel</p></a>
						</div>
					</div>
				</div>

			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
