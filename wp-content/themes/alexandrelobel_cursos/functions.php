<?php
/**
 * Cursos | Alexandre Lobel functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Cursos_|_Alexandre_Lobel
 */

if ( ! function_exists( 'alexandrelobel_cursos_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function alexandrelobel_cursos_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Cursos | Alexandre Lobel, use a find and replace
		 * to change 'alexandrelobel_cursos' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'alexandrelobel_cursos', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'alexandrelobel_cursos' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'alexandrelobel_cursos_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'alexandrelobel_cursos_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function alexandrelobel_cursos_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'alexandrelobel_cursos_content_width', 640 );
}
add_action( 'after_setup_theme', 'alexandrelobel_cursos_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function alexandrelobel_cursos_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'alexandrelobel_cursos' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'alexandrelobel_cursos' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'alexandrelobel_cursos_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function alexandrelobel_cursos_scripts() {
	//jQuery	
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

	//Bootstrap
	wp_enqueue_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js', array(), null, true);
	wp_enqueue_style( 'bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css',false,'1.1','all');

	//YouTube API
	wp_enqueue_script('youtube-js', 'https://www.youtube.com/iframe_api', array(), null, true);

	//Vendor and Custom JS
	wp_enqueue_script('vendor-min-js', get_template_directory_uri() . '/assets/js/vendor.min.js', array(), null, true);
	wp_enqueue_script('vendor-js', get_template_directory_uri() . '/assets/js/vendor.js', array(), null, true);
	wp_enqueue_script('custom-min-js', get_template_directory_uri() . '/assets/js/custom.min.js', array(), null, true);
	wp_enqueue_script('custom-js', get_template_directory_uri() . '/assets/js/custom.js', array(), null, true);

	wp_enqueue_script( 'alexandrelobel_cursos-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'alexandrelobel_cursos-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_style( 'alexandrelobel_cursos-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'alexandrelobel_cursos_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
show_admin_bar(false);

function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	echo $text;
}

//Custom Type Vídeo
function postTypeVideo() {
	register_post_type('video', array(
		'labels' => array(
			'name' => __( 'Vídeos' ),
			'singular_name' => __( 'Vídeo' )
		),
		'public' => true,
		'menu_position' => 5,
		'show_ui' => true,
		'capability_type' => 'page',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'video'),
		'menu_icon' => 'dashicons-editor-video',
		'supports' => array('title', 'thumbnail')
	));
	register_taxonomy('serie', 'video', array(
		'hierarchical' => false,
		'labels' => array(
			'name' => _x('Séries', 'taxonomy general name' ),
			'singular_name' => _x( 'Série', 'taxonomy singular name' ),
			'search_items' =>  __( 'Procurar séries' ),
			'all_items' => __( 'Todas as séries' ),
			'parent_item' => __( 'Série ascendente' ),
			'parent_item_colon' => __( 'Série ascendente' ),
			'edit_item' => __( 'Editar série' ),
			'update_item' => __( 'Atualizar série' ),
			'add_new_item' => __( 'Nova série' ),
			'new_item_name' => __( 'Nova série' ),
			'menu_name' => __( 'Séries' )
		),
		'show_ui' => false,
		'show_admin_column' => false,
		'rewrite' => array( 'slug' => 'serie')
	));
	register_taxonomy('categoria', 'video', array(
		'hierarchical' => false,
		'labels' => array(
			'name' => _x('Categorias', 'taxonomy general name' ),
			'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
			'search_items' =>  __( 'Procurar categorias' ),
			'all_items' => __( 'Todas as categorias' ),
			'parent_item' => __( 'Categoria ascendente' ),
			'parent_item_colon' => __( 'Categoria ascendente' ),
			'edit_item' => __( 'Editar categoria' ),
			'update_item' => __( 'Atualizar categoria' ),
			'add_new_item' => __( 'Nova categoria' ),
			'new_item_name' => __( 'Nova categoria' ),
			'menu_name' => __( 'Categorias' )
		),
		'show_ui' => false,
		'show_admin_column' => false,
		'rewrite' => array( 'slug' => 'categoria')
	));
}
add_action( 'init', 'postTypeVideo' );