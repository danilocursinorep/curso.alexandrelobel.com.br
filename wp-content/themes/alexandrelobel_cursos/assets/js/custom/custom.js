$(document).ready(function(){
     
});

var player;
function onYouTubePlayerAPIReady() {
     player = new YT.Player('video', {
          events: {
               'onReady': onPlayerReady,
               'onStateChange': onPlayerStateChange
          }
     });
}

function onPlayerReady(event) {
     var playButton = document.getElementById("play");
     playButton.addEventListener("click", function() {
          player.playVideo();
     });
     var pauseButton = document.getElementById("pause");
     pauseButton.addEventListener("click", function() {
          player.pauseVideo();
     });  
}

function onPlayerStateChange(event) {        
     // if(event.data === 0) {
     //      if ($('.botoes.next').length > 0) {
     //           window.location.href = $('.botoes.next').attr('link');
     //      }
     // }
}