<?php /* Template Name: Páginas política */ ?>
<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="video page" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
				<div class="container video">
					<div class="row">
						<div class="col-12">
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
			</section>

			<section class="politica">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
								while (have_posts()) {
									the_post();
									the_content();
								}
							?>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();